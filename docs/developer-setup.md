---
id: developer-setup
title: Developer Setup
sidebar_label: Developer Setup
---

Clone the code repo
```
git clone git@gitlab.com:fastxt/fastxt.git
```

SQLite database file location on macOS
```
~/Library/Containers/app.fastxt.Fastxt/Data/Fastxt/fastxt.sqlite3
```
